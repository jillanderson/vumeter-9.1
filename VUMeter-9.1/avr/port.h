#pragma once

/** \file **********************************************************************
 *
 * początkowa inicjalizacja portów wejścia-wyjścia
 *
 ******************************************************************************/

#include <avr/io.h>
#include <common/gcc_attributes.h>
#include <mega/port.h>
#include <stdint.h>

/*
 * włącza podciągnięcie wszystkich wejść do Vcc
 *
 * po resecie kontrolera PORTx, DDRx, PINx są wyzerowane, czyli ustawione jako
 * wejścia bez podciągnięcia do Vcc,
 */
__gcc_static_inline void port_setting_initial(void)
{
	static uint8_t const PULLUP_ON_FOR_ALL = 0xFFU;

	port_set_value(&PORTA, PULLUP_ON_FOR_ALL);
	port_set_value(&PORTB, PULLUP_ON_FOR_ALL);
	port_set_value(&PORTC, PULLUP_ON_FOR_ALL);
	port_set_value(&PORTD, PULLUP_ON_FOR_ALL);
}

