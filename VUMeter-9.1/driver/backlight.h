#pragma once

/** \file **********************************************************************
 *
 * obsługa podświetlania wskaźników vu
 *
 ******************************************************************************/

typedef enum {
	BRIGHTNESS_NONE,
	BRIGHTNESS_MINIMUM,
	BRIGHTNESS_LOW,
	BRIGHTNESS_MEDIUM,
	BRIGHTNESS_HIGH,
	BRIGHTNESS_MAXIMUM
} brightness_et;

/*
 * inicjalizuje obsługę podświetlenia wskaźników
 */
void backlight_init(void);

/*
 * ustawia jasność podświetlenia wskaźników
 *
 * @brightness	poziom podświetlenia
 */
void backlight_brightness_set(brightness_et brightness);

