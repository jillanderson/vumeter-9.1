#pragma once

/** \file **********************************************************************
 *
 * obsługa przycisku monostabilnego
 *
 ******************************************************************************/

#include <common/key_press_type_detector.h>
#include <stdint.h>

/*
 * inicjalizuje obsługę przycisku
 *
 * @debounce_time_ms		czas debouncingu
 * @double_press_time_ms	czas wykrycia podwójnego wciśnięcia
 * @longpress_start_time_ms	czas wykrycia długiego wciśnięcia
 * @longpress_repeat_time_ms	czas repetycji przy długim wciśnięciu
 */
void key_init(uint8_t debounce_time_ms, uint16_t double_press_time_ms,
                uint16_t longpress_start_time_ms,
                uint16_t longpress_repeat_time_ms);

/*
 * rejestruje funkcję obsługi zdarzeń
 *
 * @handler	funkcja obsługująca zdarzenia od przycisku
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 */
void key_handler(key_press_handler_ft *handler, void *arg);

/*
 * uaktualnia stan systemu wykrywania zdarzeń od przycisku
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void key_on_tick_time(void);

