#include "../../hal/key.h"
#include "../key.h"

#include <common/key_integrator_filter.h>
#include <common/key_press_type_detector.h>
#include <stdint.h>

static struct {
	key_integrator_st debouncer;
	key_press_event_detector_st event_detector;
} key = { 0 };

void key_init(uint8_t debounce_time_ms, uint16_t double_press_time_ms,
                uint16_t longpress_start_time_ms,
                uint16_t longpress_repeat_time_ms)
{
	hw_key_init();
	key_integrator_filter_init(&(key.debouncer), debounce_time_ms);
	key_press_event_detector_init(&(key.event_detector),
	                double_press_time_ms, longpress_start_time_ms,
	                longpress_repeat_time_ms);
}

void key_handler(key_press_handler_ft *handler, void *arg)
{
	key_press_event_detector_handler(&(key.event_detector), handler, arg);
}

void key_on_tick_time(void)
{
	key_integrator_filter_on_tick_time(&(key.debouncer),
	                hw_key_is_pressed());
	key_press_event_detector_on_tick_time(&(key.event_detector),
	                key_integrator_filter_read_state(&(key.debouncer)));
	key_press_event_detector_dispatch(&(key.event_detector));
}

