#include "../vumeter.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../hal/measurer.h"
#include "../../hal/source.h"

static struct {
	volatile uint8_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
} vumeter = { 0 };

void vumeter_init(void)
{
	hw_source_init();
	hw_measurer_init();
}

void vumeter_on(void)
{
	/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
	static uint8_t const RELAY_BOUNCE_TIME_MS = 15U;

	if (COUNTER_STOPPED == vumeter.counter_status) {
		if ((false == hw_source_is_on())
		                && (false == hw_measurer_is_on())) {
			hw_source_on();
			vumeter.time_counter = RELAY_BOUNCE_TIME_MS;
			vumeter.counter_status = COUNTER_RUNNING;
		}
	}
}

void vumeter_off(void)
{
	hw_source_off();
	hw_measurer_off();
	vumeter.counter_status = COUNTER_STOPPED;
}

void vumeter_on_tick_time(void)
{
	if (COUNTER_RUNNING == vumeter.counter_status) {

		if (0U < vumeter.time_counter) {
			vumeter.time_counter--;
		}

		if (0U == vumeter.time_counter) {
			hw_measurer_on();
			vumeter.counter_status = COUNTER_STOPPED;
		}
	}
}

