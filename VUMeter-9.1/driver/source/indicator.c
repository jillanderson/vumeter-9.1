#include "../../hal/indicator.h"
#include "../indicator.h"

#include <stdbool.h>
#include <stdint.h>

static struct {
	uint16_t on_time;
	uint16_t off_time;
	volatile uint16_t time_counter;
	volatile enum {
		COUNTER_STOPPED,
		COUNTER_RUNNING
	} counter_status;
} indicator = { 0 };

static void switch_state(void);

void indicator_init(void)
{
	hw_indicator_init();
}

void indicator_on(void)
{
	hw_indicator_on();
	indicator.counter_status = COUNTER_STOPPED;
}

void indicator_off(void)
{
	hw_indicator_off();
	indicator.counter_status = COUNTER_STOPPED;
}

void indicator_blink(uint16_t on_time_ms, uint16_t off_time_ms)
{
	indicator.on_time = on_time_ms;
	indicator.off_time = off_time_ms;
	switch_state();
	indicator.counter_status = COUNTER_RUNNING;
}

void indicator_on_tick_time(void)
{
	if (COUNTER_RUNNING == indicator.counter_status) {

		if (0U < indicator.time_counter) {
			indicator.time_counter--;
		}

		if (0U == indicator.time_counter) {
			switch_state();
		}
	}
}

void switch_state(void)
{
	hw_indicator_toggle();
	indicator.time_counter =
	                (true == hw_indicator_is_on()) ?
	                                indicator.on_time : indicator.off_time;
}

