#include "../../hal/backlight.h"
#include "../backlight.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

/*
 * This table remaps linear input values (the numbers we’d like to use;
 * e.g. 127 = half brightness) to nonlinear gamma-corrected output values
 * (numbers producing the desired effect on the LED; e.g. 36 = half brightness).
 */
static uint8_t const __flash gamma8[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3,
                3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8,
                9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15,
                15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23,
                24, 24, 25, 25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34,
                35, 35, 36, 37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                49, 50, 50, 51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64,
                66, 67, 68, 69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85,
                86, 87, 89, 90, 92, 93, 95, 96, 98, 99, 101, 102, 104, 105, 107,
                109, 110, 112, 114, 115, 117, 119, 120, 122, 124, 126, 127, 129,
                131, 133, 135, 137, 138, 140, 142, 144, 146, 148, 150, 152, 154,
                156, 158, 160, 162, 164, 167, 169, 171, 173, 175, 177, 180, 182,
                184, 186, 189, 191, 193, 196, 198, 200, 203, 205, 208, 210, 213,
                215, 218, 220, 223, 225, 228, 231, 233, 236, 239, 241, 244, 247,
                249, 252, 255 };

static struct backlight {
	uint16_t compare;
} backlight = { 0 };

static void timer_start(uint8_t prescaler_mask, uint16_t top_value);
static void timer_stop(void);

void backlight_init(void)
{
	hw_backlight_init();
}

void backlight_brightness_set(brightness_et brightness)
{
	/* konfiguruje częstotliwość odświerzania 100Hz */
	static uint8_t const PRESCALER_8 = 2U;
	static uint16_t const TOP = 9999U;
	/* (255 / 51) = 5, [0-51-102-153-204-255] */
	static uint8_t const BRIGHTNESS_STEP = 51U;

	timer_stop();

	switch (brightness) {
	case BRIGHTNESS_NONE:
		hw_backlight_off();
		break;

	case (BRIGHTNESS_NONE + 1U) ... (BRIGHTNESS_MAXIMUM - 1U):
		backlight.compare = (uint16_t) (((uint32_t) TOP
		                * gamma8[(brightness * BRIGHTNESS_STEP)])
		                / UINT8_MAX);
		timer_start(PRESCALER_8, TOP);
		break;

	case BRIGHTNESS_MAXIMUM:
		hw_backlight_on();
		break;

	default:
		;
		break;
	}
}

void timer_start(uint8_t prescaler_mask, uint16_t top_value)
{
	TCCR1A = 0U;
	TCCR1B = (uint8_t) ((1U << WGM12) + prescaler_mask);
	TIFR |= (1U << OCF1A); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIFR |= (1U << OCF1B); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK |= (1U << OCIE1A);
	TIMSK |= (1U << OCIE1B);
	OCR1A = top_value;
	OCR1B = 0U;
	TCNT1 = 0U;
}

void timer_stop(void)
{
	TCCR1A = 0U;
	TCCR1B = 0U;
	TIFR |= (1U << OCF1A); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIFR |= (1U << OCF1B); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK &= (uint8_t) (~(1U << OCIE1A));
	TIMSK &= (uint8_t) (~(1U << OCIE1B));
}

/*
 * przerwanie od przepełnienia TOP
 */
ISR(TIMER1_COMPA_vect)
{
	hw_backlight_on();

	OCR1B = backlight.compare;
}

/*
 * przerwanie od porównania COMPARE
 */
ISR(TIMER1_COMPB_vect)
{
	hw_backlight_off();
}

