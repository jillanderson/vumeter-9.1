/** \file **********************************************************************
 *
 * Sterownik miernika VUmeter v9.1
 *
 * Copyright (C) 2022r. GNU Public License
 *
 * zzbych2547<at>linux.pl
 *
 * funkcje:
 * 	- sterowanie włączaniem i wyłączaniem
 * 	- sterowanie podświetleniem wskaźników
 *
 * sterowanie:
 * 	- krótkie naciśnięcie przycisku: włącz/wyłącz
 * 	- podwójne lub długie naciśnięcie przycisku: zmiana jasności
 * 		podświetlenia w pętli od min do max
 *
 * zasoby sprzętowe:
 * 	- CPU			ATmega32
 * 	- F_CPU			8MHz oscylator wewnętrzny
 * 	- BOD			max 4,5V
 * 	- TIMER2		systemowa podstawa czasu
 * 	- TIMER1		regulacja podświetlania
 *
 * ustawienia kompilatora gcc 12.1.0:
 *
 * opcje avr
 * ---------
 * 	-mrelax			dodaje opcje --mlink-relax do linii
 * 				komend assemblera oraz --relax do linii komend
 * 				linkera)
 * 	-mcall-prologues	rozwija prologi/epilogi funkcji w odrębne
 * 				podprogramy
 * 	-maccumulate-args	może prowadzić do zmniejszenia rozmiaru kodu
 * 				dla funkcji, które wykonują kilka wywołań
 *				i które otrzymują swoje argumenty na stosie
 * 	-mstrict-X		użycie rejestru X w sposób proponowany przez
 * 				sprzęt. Używany w adresowaniu pośrednim.
 * 				(tu: nieużywana)
 *
 * opcje gcc
 * ---------
 * 	-std=gnu99		standard c99 i rozszerzenia gnu
 * 	-03			poziom optymalizacji
 * 	-fshort-enums		najkrótszy typ mieszczący wszystkie wartości
 *	-ffunction-sections	odrębne sekcje dla funkcji
 *	-fdata-sections		odrębne sekcje dla danych
 *	-funsigned-char		typ char bez znaku
 *	-funsigned-bitfields	pola bitowe bez znaku
 *	-flto			link time optimization
 *
 * podstawowe ostrzeżenia
 * ----------------------
 *	-Wall
 *	-Wextra
 *	-Werror
 *
 * dodatkowe ostrzeżenia nie włączane przez -Wall -Wextra
 * ------------------------------------------------------
 * 	-Walloc-zero
 * 	-Warith-conversion
 * 	-Wbad-function-cast
 * 	-Wcast-qual
 * 	-Wconversion
 * 	-Wdouble-promotion
 * 	-Wduplicated-branches
 * 	-Wduplicated-cond
 * 	-Wfloat-conversion
 * 	-Wfloat-equal
 * 	-Winit-self
 * 	-Wjump-misses-init
 * 	-Wlogical-op
 * 	-Wmissing-declarations
 * 	-Wmissing-include-dirs
 * 	-Wmissing-prototypes
 * 	-Wmultichar
 * 	-Wnested-externs
 * 	-Wopenacc-parallelism
 * 	-Woverlength-strings
 * 	-Wpointer-arith
 * 	-Wredundant-decls
 * 	-Wsign-conversion
 * 	-Wstrict-prototypes
 * 	-Wsuggest-attribute=cold
 * 	-Wsuggest-attribute=const
 * 	-Wsuggest-attribute=malloc
 * 	-Wsuggest-attribute=noreturn
 * 	-Wsuggest-attribute=pure
 * 	-Wswitch-default
 * 	-Wundef
 * 	-Wunsuffixed-float-constants
 * 	-Wunused-macros
 * 	-Wvariadic-macros
 * 	-Wwrite-strings
 *	-Wcast-align
 *	-Winline
 *	-Wmissing-format-attribute
 *	-Wnull-dereference
 *	-Wpacked
 *	-Wpadded
 *	-Wrestrict
 *	-Wshadow
 *	-Wunsafe-loop-optimizations
 *
 * opcje linkera
 * -------------
 *	-Wl,--gc-sections,--warn-common
 *
 ******************************************************************************/

#include <common/gcc_attributes.h>
#include <stdlib.h>

#include "device/device.h"

 __gcc_os_main int main(void)
{
	device_init();

	for (;;) {
		device_handle_events();
	}

	return (EXIT_SUCCESS);
}

