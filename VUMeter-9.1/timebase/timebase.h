#pragma once

/** \file **********************************************************************
 *
 * podstawa czasu aplikacji - 1ms
 *
 ******************************************************************************/

typedef void timebase_handler_ft(void *arg);

/*
 * uruchamia timer w trybie CTC, włącza przerwanie TIMER2_COMP_vect,
 * rozdzielczość podstawy czasu: 1ms (1kHz)
 */
void timebase_start(void);

/*
 * rejestruje handler obsługi podstawy czasu
 *
 * @handler	funkcja wykonywana w przerwaniu timera
 * @arg		argument handlera
 */
void timebase_handler(timebase_handler_ft *handler, void *arg);

