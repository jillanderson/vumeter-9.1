#pragma once

/** \file **********************************************************************
 *
 * sterowanie podświetleniem wskaźników VU
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje podświetlenie wskaźników wychyłowych
 */
void hw_backlight_init(void);

/*
 * włącza podświetlenie wskaźników wychyłowych
 */
void hw_backlight_on(void);

/*
 * wyłącza podświetlenie wskaźników wychyłowych
 */
void hw_backlight_off(void);

/*
 * sprawdza czy podświetlenie wskaźników wychyłowych jest włączone
 *
 * @ret		true - podświetlenie włączone
 *		false - podświetlenie wyłączone
 */
bool hw_backlight_is_on(void);

