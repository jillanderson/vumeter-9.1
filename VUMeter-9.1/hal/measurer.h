#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikiem podłączającym miernik do sygnału wejściowego
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje miernik sygnału
 */
void hw_measurer_init(void);

/*
 * przyłącza miernik do sygnału mierzonego
 */
void hw_measurer_on(void);

/*
 * odłącza miernik od sygnału mierzonego
 */
void hw_measurer_off(void);

/*
 * sprawdza czy miernik jest przyłączony do sygnału mierzonego
 *
 * @ret		true - miernik przyłączony
 *		false - miernik nie przyłączony
 */
bool hw_measurer_is_on(void);

