#pragma once

/** \file **********************************************************************
 *
 * sterowanie przekaźnikami podłączającymi sygnał wejściowy
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjuje sterowanie źródłem sygnału
 */
void hw_source_init(void);

/*
 * podłącza źródło sygnału
 */
void hw_source_on(void);

/*
 * odłącza źródło sygnału
 */
void hw_source_off(void);

/*
 * sprawdza czy źródło sygnału jest podłączone
 *
 * @ret		true - źródło podłączone
 * 		false - źródło nie podłączone
 */
bool hw_source_is_on(void);

