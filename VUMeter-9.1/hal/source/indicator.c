#include "../indicator.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const LED_INDICATOR = { .port = &PORTC, .pin = PC4 };

void hw_indicator_init(void)
{
	io_configure_as_output(&LED_INDICATOR);
	io_set_low(&LED_INDICATOR);
}

void hw_indicator_on(void)
{
	io_set_high(&LED_INDICATOR);
}

void hw_indicator_off(void)
{
	io_set_low(&LED_INDICATOR);
}

void hw_indicator_toggle(void)
{
	io_set_opposite(&LED_INDICATOR);
}

bool hw_indicator_is_on(void)
{
	return ((0U == io_read_pin(&LED_INDICATOR)) ? false : true);
}

