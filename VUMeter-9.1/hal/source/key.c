#include "../key.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const KEY = (io_pin_st ) { .port = &PORTB, .pin = PB0 };

void hw_key_init(void)
{
	io_configure_as_input(&KEY);
	io_set_low(&KEY);
}

bool hw_key_is_pressed(void)
{
	return ((0U == io_read_pin(&KEY)) ? true : false);
}

