#include "../source.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_SOURCE_0 = { .port = &PORTA, .pin = PA4 };
static io_pin_st const RELAY_SOURCE_1 = { .port = &PORTA, .pin = PA5 };

static bool source_0_is_on(void);
static bool source_1_is_on(void);

void hw_source_init(void)
{
	io_configure_as_output(&RELAY_SOURCE_0);
	io_configure_as_output(&RELAY_SOURCE_1);
	io_set_low(&RELAY_SOURCE_0);
	io_set_low(&RELAY_SOURCE_1);
}

void hw_source_on(void)
{
	io_set_high(&RELAY_SOURCE_0);
	io_set_high(&RELAY_SOURCE_1);
}

void hw_source_off(void)
{
	io_set_low(&RELAY_SOURCE_0);
	io_set_low(&RELAY_SOURCE_1);
}

bool hw_source_is_on(void)
{
	return (((true == source_0_is_on()) && (true == source_1_is_on())) ?
	                true : false);
}

bool source_0_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_SOURCE_0)) ? false : true);
}

bool source_1_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_SOURCE_1)) ? false : true);
}

