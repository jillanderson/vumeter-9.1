#include "../backlight.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const LED_BACKLIGHT_LEFT = { .port = &PORTC, .pin = PC3 };
static io_pin_st const LED_BACKLIGHT_RIGHT = { .port = &PORTA, .pin = PA0 };

static bool backlight_left_is_on(void);
static bool backlight_right_is_on(void);

void hw_backlight_init(void)
{
	io_configure_as_output(&LED_BACKLIGHT_LEFT);
	io_configure_as_output(&LED_BACKLIGHT_RIGHT);
	io_set_low(&LED_BACKLIGHT_LEFT);
	io_set_low(&LED_BACKLIGHT_RIGHT);
}

void hw_backlight_on(void)
{
	io_set_high(&LED_BACKLIGHT_LEFT);
	io_set_high(&LED_BACKLIGHT_RIGHT);
}

void hw_backlight_off(void)
{
	io_set_low(&LED_BACKLIGHT_LEFT);
	io_set_low(&LED_BACKLIGHT_RIGHT);
}

bool hw_backlight_is_on(void)
{
	return (((true == backlight_left_is_on())
	                && (true == backlight_right_is_on())) ? true : false);
}

bool backlight_left_is_on(void)
{
	return ((0U == io_read_pin(&LED_BACKLIGHT_LEFT)) ? false : true);
}

bool backlight_right_is_on(void)
{
	return ((0U == io_read_pin(&LED_BACKLIGHT_RIGHT)) ? false : true);
}

