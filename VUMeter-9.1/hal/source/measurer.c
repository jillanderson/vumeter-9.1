#include "../measurer.h"

#include <avr/io.h>
#include <mega/io.h>
#include <stdbool.h>

static io_pin_st const RELAY_MUTE = { .port = &PORTC, .pin = PC7 };

void hw_measurer_init(void)
{
	io_configure_as_output(&RELAY_MUTE);
	io_set_low(&RELAY_MUTE);
}

void hw_measurer_on(void)
{
	io_set_high(&RELAY_MUTE);
}

void hw_measurer_off(void)
{
	io_set_low(&RELAY_MUTE);
}

bool hw_measurer_is_on(void)
{
	return ((0U == io_read_pin(&RELAY_MUTE)) ? false : true);
}

